/*
------------------------------------------------------------------
 Description: Simple program illustrating the use of the lzOG
              Lempel_Ziv factorisation library to factorise a file
 Author: Angelos Molfetas (2013)
 Copyright: The University of Melbourne (2013)
 Licence: BSD licence, see attached LICENCE file
 -----------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <vector>
#include <iostream>
#include <list>

#include "KKreps.hpp"

using namespace std;


int simplefileOpen(const char *filename,unsigned long *numofbytes)
{
  int fd;
  struct stat buf;

  fd = open(filename,O_RDONLY); /* open file for read, get filedes */
  if (fd == -1) /* check for error code */
  {
    fprintf(stderr,"Cannot open \"%s\"\n",filename);
    return -1;
  }
  if (fstat(fd,&buf) == -1)  /* get status information of file */
  {
    fprintf(stderr,"Cannot access status of file: %s\n",filename);
    return -2;
  }
  *numofbytes = (unsigned long)buf.st_size; /* store file size in address of numofbytes */
  return fd;
}


void *creatememorymap(const char *filename,
                      unsigned long *numofbytes)
{
  int fd;
  void *memorymap;

  fd = simplefileOpen(filename,numofbytes);
  if (fd < 0)
  {
    return NULL;
  }
  memorymap = mmap(NULL, /* no address specified for map */
                   (size_t) *numofbytes,
                   PROT_READ, /* pages may be read */
                   MAP_PRIVATE, /* unclear why need for reading */
                   fd, /* file descriptor */
                   (off_t) 0); /* offset: multiple of page size */
  if (memorymap == (void *) MAP_FAILED)
  {
    fprintf(stderr,"%s(%s) failed\n",__func__,filename);
    exit(EXIT_FAILURE);
  }
  return memorymap;
}


void deletememorymap(void *memorymap,unsigned long numofbytes)
{
  if (munmap(memorymap,(size_t) numofbytes) != 0)
  {
    fprintf(stderr,"%s failed\n",__func__);
    exit(EXIT_FAILURE);
  }
}


int main(int argc, const char **argv)
{
   set<Repetition> reps;    // Store the repetitions as tuples (start, end, period)

   if (argc != 2) // Validate input
     {
       fprintf(stderr,"Usage: %s <inputfile>\n",argv[0]);
       exit(EXIT_FAILURE);
     }

   unsigned long *filecontents; // File contents will be stored here
   long unsigned int numofbytes;
   filecontents = (unsigned long*)creatememorymap(argv[1], &numofbytes);
   kk_compute_repetitions(numofbytes/8, filecontents, reps);

   list<Repetition> replist;
   for ( set<Repetition>::iterator r_it = reps.begin(); r_it != reps.end(); r_it++ )
     replist.push_back(*r_it);
   replist.sort();
   for ( list<Repetition>::iterator l_it = replist.begin(); l_it != replist.end(); l_it++ )
     {
       cout << l_it->start << " " << l_it->end << " " << l_it->period << " [";
       for ( unsigned pos = l_it->start; pos <= l_it->end; pos++ )
	 {
	   if ( pos > l_it->start )
	     cout << ", ";
	   cout << filecontents[pos];
	 }
       cout << "]\n";
     }

   deletememorymap(filecontents, numofbytes);
   
   return 0;
}
