#!/usr/bin/env python2.7

import tempfile
import sys
import os
import struct
import subprocess


def find_maximal_repetitions(vec, mrpath = "./max_rep"):
    endsym = len(vec)
    elem_map = {}
    distinct_count = 0;
    ## (__inF, inFName) = tempfile.mkstemp()
    ## os.close(__inF)
    ## inF = open(inFName, 'w')
    inF = open("inFile.txt", 'w')
    # Map each vector element to a unique integer
    for i in range(0, len(vec)):
        if vec[i] in elem_map:
            obj_id = elem_map[ vec[i] ]
        else:
            obj_id = distinct_count
            elem_map[ vec[i] ] = distinct_count
            distinct_count += 1
        inF.write(struct.pack("Q", obj_id))
    inF.write(struct.pack("Q", endsym))
    inF.close()
    ## (__outF, outFName) = tempfile.mkstemp()
    ## outF = os.fdopen(__outF)
    outF = open("outFile.txt", 'w')
    ## retval = subprocess.call([mrpath, inFName], stdout=outF)
    retval = subprocess.call([mrpath, "inFile.txt"], stdout=outF)
    outF.close()
    ## os.unlink(inFName)
    if retval != "0":
        repList = []
        ## outF = open(outFName, 'r')
        outF = open("outFile.txt", 'r')
        for line in outF:
            meta = [int(x) for x in line[:line.find('[')].split()]
            seq = [vec[int(x)] for x in line[line.find('[')+1:-2].split(',')]
            repList.append((meta, seq))
        outF.close()
        ## os.unlink(outFName)
        return repList
    else:
        ## os.unlink(outFName)
        raise Exception("Internal error")

if __name__ == "__main__":
    # testseq = ["please", "read", "this", "please", "read", "this", "please", "read", "this", "software", "license", "agreement"]

    # print "Input sequence:", testseq
    # reps = find_maximal_repetitions(testseq)
    # for r in reps:
    #     meta = r[0]
    #     seq = r[1]
    #     print("Start: %d, End: %d, Period: %d, Seq: %s" % (meta[0], meta[1], meta[2], str(seq)))
    seq = []
    for line in open("tests/input.trace", 'r'):
        seq.append(' '.join(line.rstrip('\n').split()[1:]))
    reps = find_maximal_repetitions(seq)
    for r in reps:
        meta = r[0]
        rep = r[1]
        print("Start: %d, End: %d, Period: %d, Seq: %s" % (meta[0], meta[1], meta[2], str(seq[meta[0]:meta[0]+meta[2]])))
        
    

        

        
        
