#ifndef __LONGEST_EXT_HPP
#define __LONGEST_EXT_HPP

void LP(unsigned long len, const unsigned long* input, vector<unsigned long>& results);
void LS(unsigned long len, const unsigned long* input, vector<unsigned long>& results);
void LPvw(unsigned long len, unsigned long len_v, const unsigned long* vw_word, vector<unsigned long>& lpv);
void LSwv(unsigned long len, unsigned long len_v, const unsigned long* wv_word, vector<unsigned long>& lsv);

#endif
