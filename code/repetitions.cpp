#include "repetitions.hpp"
#include "longest-ext.hpp"
#include <iostream>


void right_repetitions(unsigned long orig_offset, const unsigned long* xyword,
		       unsigned long len_xy, unsigned long len_y, set<Repetition>& res)
{
  unsigned long len_x = len_xy - len_y;
  
  vector<unsigned long> LPy;
  LP(len_y, &xyword[len_x], LPy);
  vector<unsigned long> LSxy;
  LSwv(len_xy, len_y, xyword, LSxy);

  // Note, LPy is built with an additional 0 at the end to ensure the
  // following loop remains correct when p+1 = len_y
  for ( unsigned long p = 0; p < len_y; p++ )
    {
      if ( LSxy[p] + LPy[p+1] > p )
	{
	  // cout << "Found repetition at position " << p << ": " << len_x-LSxy[p]+orig_offset << ", " << len_x+p+LPy[p+1]+orig_offset << ", " << p+1 << endl;

	  // Note, because of Lothaire's algorithm design we must skip
	  // repetitions that end on the last symbol of xy - those will
	  // be found in the next iteration of the main algorithm loop
	  unsigned long start_pos = len_x-LSxy[p];
	  unsigned long end_pos = len_x+p+LPy[p+1];
	  if ( end_pos < len_xy-1 )
	    res.insert(Repetition(start_pos+orig_offset, end_pos+orig_offset,p+1));
	}
    }
}


void left_repetitions(unsigned long orig_offset, const unsigned long* xyword,
		      unsigned long len_xy, unsigned long len_x, set<Repetition>& res)
{
  vector<unsigned long> LSx;
  LS(len_x, xyword, LSx);
  vector<unsigned long> LPyx;
  LPvw(len_xy, len_x, xyword, LPyx);

  // Note, loop goes backward so at the end of the loop only the
  // repetition instance with the smallest period gets inserted

  // x vector beyond initial element (would require the loop to access
  // LSx[-1], which would not work, so that case is managed separately
  for ( unsigned long h = 1; h < len_x; h++ )
    {
      unsigned long i = len_x - h;
      if ( LSx[i-1] + LPyx[i] >= (len_x-i) )
	{
	  // cout << "Found repetition at position " << i << ": " << i-LSx[i-1]+orig_offset << ", " << len_x+LPyx[i]-1+orig_offset << ", " << len_x-i << endl;

	  // Note, because of Lothaire's algorithm design we must skip
	  // repetitions that end on the last symbol of xy - those will
	  // be found in the next iteration of the main algorithm loop
	  unsigned long start_pos = i-LSx[i-1];
	  unsigned long end_pos = len_x+LPyx[i]-1;
	  if ( end_pos < len_xy-1 )
	    res.insert(Repetition(start_pos+orig_offset,end_pos+orig_offset,len_x-i));
	}
    }
  // "-1" case (here we cannot do the vector-extension trick as in
  // right_repetitions, since C++ vectors do not support negative
  // indexing
  if ( LPyx[0] >= len_x ) {
    // cout << "Found repetition at position " << 0 << ": " << orig_offset << ", " << len_x+LPyx[0]-1+orig_offset << ", " << len_x << endl;
    unsigned start_pos = 0;
    unsigned end_pos = len_x+LPyx[0]-1;
    if ( end_pos < len_xy-1 )
      res.insert(Repetition(start_pos+orig_offset, end_pos+orig_offset, len_x));
  }

}

// void print_vector(unsigned long* vec, unsigned len, unsigned split)
// {
//   for ( unsigned i = 0; i < split; i++ )
//     cout << vec[i] << " ";
//   cout << "| ";
//   for ( unsigned i = split; i < len; i++ )
//     cout << vec[i] << " ";
//   cout << endl;
// }

// void print_repetitions(unsigned long* word, set<Repetition>& rep)
// {
//   for ( set<Repetition>::iterator r_it = rep.begin(); r_it != rep.end(); r_it++ )
//     {
//       cout << "Start pos: " << (*r_it).start << "; end pos: " << (*r_it).end << "; period:" << (*r_it).period << "; content:";
//       for ( unsigned i = (*r_it).start; i <= (*r_it).end; i++ )
// 	{
// 	  cout << " " << word[i];
// 	}
//       cout << endl << "------------" << endl;
//     }
//   cout << endl;
// }


// int main()
// {
//   unsigned long vec1_1[] = {1,1,1,1,1, 4,5,6,4,5,6,7,8};
//   set<Repetition> res1_1;
//   unsigned long vec1_2[] = {1,1,1,1,1, 2,2,2,2,2,2,2,2};
//   set<Repetition> res1_2;
//   unsigned long vec1_3[] = {1,1,1,3,4, 5,3,4,5,3,4,5,3};
//   set<Repetition> res1_3;
//   unsigned long vec1_4[] = {3,4,5,3,4, 5,3,4,5,3,4,5,3};
//   set<Repetition> res1_4;
//   unsigned long vec1_5[] = {1,1,1,1,1,1,1,1, 4,5,4,5,8};
//   set<Repetition> res1_5;
//   unsigned long vec1_6[] = {1,1,1,1,1,1,1,1, 1,2,2,2,2};
//   set<Repetition> res1_6;
//   unsigned long vec1_7[] = {1,1,1,1,1,1,3,4, 5,3,4,5,3};
//   set<Repetition> res1_7;
//   unsigned long vec1_8[] = {3,4,5,3,4,5,3,4, 5,3,4,5,3};
//   set<Repetition> res1_8;
//   unsigned long vec1_9[] = {1,1,1,1,1, 4,5,4,5,8};
//   set<Repetition> res1_9;
//   unsigned long vec1_10[] = {1,1,1,1,1, 2,2,2,2,2};
//   set<Repetition> res1_10;
//   unsigned long vec1_11[] = {1,1,1,3,4, 5,3,4,5,3};
//   set<Repetition> res1_11;
//   unsigned long vec1_12[] = {3,4,5,3,4, 5,3,4,5,3};
//   set<Repetition> res1_12;
//   unsigned long vec1_13[] = {4,4,4,4,4,4,4,4, 1,2,3,1,2,3,1,2,1,2};
//   set<Repetition> res1_13;
//   unsigned long vec1_14[] = {1,1,1,1,1,1,1,1, 1,2,3,1,2,3,1,2,1,2,1,2,5,6};
//   set<Repetition> res1_14;
//   unsigned long vec1_15[] = {1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1};
//   set<Repetition> res1_15;

  
//   right_repetitions(0, vec1_1, 13, 8, res1_1);
//   print_vector(vec1_1, 13, 5);
//   print_repetitions(vec1_1, res1_1);

//   right_repetitions(0, vec1_2, 13, 8, res1_2);
//   print_vector(vec1_2, 13, 5);
//   print_repetitions(vec1_2, res1_2);

//   right_repetitions(0, vec1_3, 13, 8, res1_3);
//   print_vector(vec1_3, 13, 5);
//   print_repetitions(vec1_3, res1_3);

//   right_repetitions(0, vec1_4, 13, 8, res1_4);
//   print_vector(vec1_4, 13, 5);
//   print_repetitions(vec1_4, res1_4);

//   right_repetitions(0, vec1_5, 13, 5, res1_5);
//   print_vector(vec1_5, 13, 8);
//   print_repetitions(vec1_5, res1_5);

//   right_repetitions(0, vec1_6, 13, 5, res1_6);
//   print_vector(vec1_6, 13, 8);
//   print_repetitions(vec1_6, res1_6);

//   right_repetitions(0, vec1_7, 13, 5, res1_7);
//   print_vector(vec1_7, 13, 8);
//   print_repetitions(vec1_7, res1_7);

//   right_repetitions(0, vec1_8, 13, 5, res1_8);
//   print_vector(vec1_8, 13, 8);
//   print_repetitions(vec1_8, res1_8);  

//   right_repetitions(0, vec1_9, 10, 5, res1_9);
//   print_vector(vec1_9, 10, 5);
//   print_repetitions(vec1_9, res1_9);

//   right_repetitions(0, vec1_10, 10, 5, res1_10);
//   print_vector(vec1_10, 10, 5);
//   print_repetitions(vec1_10, res1_10);

//   right_repetitions(0, vec1_11, 10, 5, res1_11);
//   print_vector(vec1_11, 10, 5);
//   print_repetitions(vec1_11, res1_11);

//   right_repetitions(0, vec1_12, 10, 5, res1_12);
//   print_vector(vec1_12, 10, 5);
//   print_repetitions(vec1_12, res1_12);

//   right_repetitions(0, vec1_13, 18, 10, res1_13);
//   print_vector(vec1_13, 18, 8);
//   print_repetitions(vec1_13, res1_13);

//   right_repetitions(0, vec1_14, 22, 14, res1_14);
//   print_vector(vec1_14, 22, 8);
//   print_repetitions(vec1_14, res1_14);

//   right_repetitions(0, vec1_15, 22, 15, res1_15);
//   print_vector(vec1_15, 22, 8);
//   print_repetitions(vec1_15, res1_15);

//   cout << "======================================================" << endl;
//   cout << "======================================================" << endl;
  
//   unsigned long vec2_1[] = { 1, 2, 3, 1, 2, 3, 1, 2,   4, 5, 6, 7, 8, 9,10,11,12,13};
//   set<Repetition> res2_1;
//   unsigned long vec2_2[] = { 1, 1, 1, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
//   set<Repetition> res2_2;
//   unsigned long vec2_3[] = { 1, 1, 1, 1, 1, 1, 1, 1,   4, 5, 6, 7, 8, 9,10,11,12,13};
//   set<Repetition> res2_3;
//   unsigned long vec2_4[] = { 1, 2, 3, 4, 5, 6, 7, 8,   1, 2, 3, 4, 5, 6, 7, 8, 9,10};
//   set<Repetition> res2_4;
//   unsigned long vec2_5[] = { 1, 2, 3, 4, 5, 1, 2, 3,   4, 5, 6, 1, 2, 3, 4, 5, 6, 7};
//   set<Repetition> res2_5;
//   unsigned long vec2_6[] = {12,13,14, 1, 2, 3, 4, 1,   2, 3, 4, 1, 2,12,12,12,12,12};
//   set<Repetition> res2_6;
//   unsigned long vec2_7[] = { 1, 2, 1, 2, 1, 2, 1, 2,   1, 2, 1, 2, 1, 3, 3, 3, 3, 3};
//   set<Repetition> res2_7;

//   unsigned long vec2_8[] = { 1, 2, 3, 1, 2, 3, 1, 2, 1, 2,  6, 7, 8, 9,10,11,12,13};
//   set<Repetition> res2_8;
//   unsigned long vec2_9[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1};
//   set<Repetition> res2_9;
//   unsigned long vec2_10[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  6, 7, 8, 9,10,11,12,13};
//   set<Repetition> res2_10;
//   unsigned long vec2_11[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9,10,  1, 2, 3, 4, 5, 6, 7, 8};
//   set<Repetition> res2_11;
//   unsigned long vec2_12[] = { 1, 2, 3, 4, 5, 1, 2, 3, 4, 5,  6, 1, 2, 3, 4, 5, 6, 7};
//   set<Repetition> res2_12;
//   unsigned long vec2_13[] = {12,13,14, 1, 2, 3, 4, 1, 2, 3,  4, 1, 2,12,12,12,12,12};
//   set<Repetition> res2_13;
//   unsigned long vec2_14[] = { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2,  1, 2, 1, 3, 3, 3, 3, 3};
//   set<Repetition> res2_14;

//   unsigned long vec2_15[] = { 1, 2, 3, 1, 2, 3, 1, 2,  6, 7, 8, 9,10,11,12,13};
//   set<Repetition> res2_15;
//   unsigned long vec2_16[] = { 1, 1, 1, 1, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1};
//   set<Repetition> res2_16;
//   unsigned long vec2_17[] = { 1, 1, 1, 1, 1, 1, 1, 1,  6, 7, 8, 9,10,11,12,13};
//   set<Repetition> res2_17;
//   unsigned long vec2_18[] = { 1, 2, 3, 4, 5, 6, 7, 8,  9,10, 1, 2, 3, 4, 5, 6};
//   set<Repetition> res2_18;
//   unsigned long vec2_19[] = { 1, 2, 3, 4, 5, 1, 2, 3,  6, 1, 2, 3, 4, 5, 6, 7};
//   set<Repetition> res2_19;
//   unsigned long vec2_20[] = {12,13,14, 1, 2, 3, 4, 1,  2, 3, 4, 1, 2,12,12,12};
//   set<Repetition> res2_20;
//   unsigned long vec2_21[] = { 1, 2, 1, 2, 1, 2, 1, 2,  1, 2, 1, 3, 3, 3, 3, 3};
//   set<Repetition> res2_21;
//   unsigned long vec2_22[] = { 1, 2, 3, 1, 2, 3, 1, 2,  3, 2, 3, 2, 3, 2, 3, 3};
//   set<Repetition> res2_22;
//   unsigned long vec2_23[] = { 2, 3, 1, 2, 3, 1, 2, 3,  3, 3, 3, 3, 3, 3, 3, 3};
//   set<Repetition> res2_23;
  
  
//   left_repetitions(0, vec2_1, 18, 8, res2_1);
//   print_vector(vec2_1, 18, 8);
//   print_repetitions(vec2_1, res2_1);

//   left_repetitions(0, vec2_2, 18, 8, res2_2);
//   print_vector(vec2_2, 18, 8);
//   print_repetitions(vec2_2, res2_2);

//   left_repetitions(0, vec2_3, 18, 8, res2_3);
//   print_vector(vec2_3, 18, 8);
//   print_repetitions(vec2_3, res2_3);

//   left_repetitions(0, vec2_4, 18, 8, res2_4);
//   print_vector(vec2_4, 18, 8);
//   print_repetitions(vec2_4, res2_4);

//   left_repetitions(0, vec2_5, 18, 8, res2_5);
//   print_vector(vec2_5, 18, 8);
//   print_repetitions(vec2_5, res2_5);

//   left_repetitions(0, vec2_6, 18, 8, res2_6);
//   print_vector(vec2_6, 18, 8);
//   print_repetitions(vec2_6, res2_6);

//   left_repetitions(0, vec2_7, 18, 8, res2_7);
//   print_vector(vec2_7, 18, 8);
//   print_repetitions(vec2_7, res2_7);


//   left_repetitions(0, vec2_8, 18, 10, res2_8);
//   print_vector(vec2_8, 18, 10);
//   print_repetitions(vec2_8, res2_8);

//   left_repetitions(0, vec2_9, 18, 10, res2_9);
//   print_vector(vec2_9, 18, 10);
//   print_repetitions(vec2_9, res2_9);

//   left_repetitions(0, vec2_10, 18, 10, res2_10);
//   print_vector(vec2_10, 18, 10);
//   print_repetitions(vec2_10, res2_10);

//   left_repetitions(0, vec2_11, 18, 10, res2_11);
//   print_vector(vec2_11, 18, 10);
//   print_repetitions(vec2_11, res2_11);

//   left_repetitions(0, vec2_12, 18, 10, res2_12);
//   print_vector(vec2_12, 18, 10);
//   print_repetitions(vec2_12, res2_12);

//   left_repetitions(0, vec2_13, 18, 10, res2_13);
//   print_vector(vec2_13, 18, 10);
//   print_repetitions(vec2_13, res2_13);

//   left_repetitions(0, vec2_14, 18, 10, res2_14);
//   print_vector(vec2_14, 18, 10);
//   print_repetitions(vec2_14, res2_14);


//   left_repetitions(0, vec2_15, 16, 8, res2_15);
//   print_vector(vec2_15, 16, 8);
//   print_repetitions(vec2_15, res2_15);

//   left_repetitions(0, vec2_16, 16, 8, res2_16);
//   print_vector(vec2_16, 16, 8);
//   print_repetitions(vec2_16, res2_16);

//   left_repetitions(0, vec2_17, 16, 8, res2_17);
//   print_vector(vec2_17, 16, 8);
//   print_repetitions(vec2_17, res2_17);

//   left_repetitions(0, vec2_18, 16, 8, res2_18);
//   print_vector(vec2_18, 16, 8);
//   print_repetitions(vec2_18, res2_18);

//   left_repetitions(0, vec2_19, 16, 8, res2_19);
//   print_vector(vec2_19, 16, 8);
//   print_repetitions(vec2_19, res2_19);

//   left_repetitions(0, vec2_20, 16, 8, res2_20);
//   print_vector(vec2_20, 16, 8);
//   print_repetitions(vec2_20, res2_20);

//   left_repetitions(0, vec2_21, 16, 8, res2_21);
//   print_vector(vec2_21, 16, 8);
//   print_repetitions(vec2_21, res2_21);

//   left_repetitions(0, vec2_22, 16, 8, res2_22);
//   print_vector(vec2_22, 16, 8);
//   print_repetitions(vec2_22, res2_22);

//   left_repetitions(0, vec2_23, 16, 8, res2_23);
//   print_vector(vec2_23, 16, 8);
//   print_repetitions(vec2_23, res2_23);

  
//   return 0;
// }
