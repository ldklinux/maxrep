#!/usr/bin/env python2.7

import sys
import os
import random
import struct

random.seed()

if len(sys.argv) != 4:
    sys.stderr.write("Usage: generate_seq.py <output file> <alphabet size> <#samples>" + os.linesep*2)
    sys.exit(-1)

with open(sys.argv[1], 'w') as outF:
    n_symbols = int(sys.argv[2]);
    n_samples = int(sys.argv[3]);
    end_symbol = n_symbols;
    for i in range(0, n_samples):
        sample = random.randint(0, n_symbols-1)
        outF.write(struct.pack('Q', sample))
    outF.write(struct.pack('Q', end_symbol))
