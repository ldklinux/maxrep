#!/usr/bin/env python2.7

import sys
import os
import random
import struct

random.seed()

if len(sys.argv) < 3:
    sys.stderr.write("Usage: generate_seq_from_args.py <output file> <space-separated samples>" + os.linesep*2)
    sys.exit(-1)

with open(sys.argv[1], 'w') as outF:
    seen_samples = set()
    for i in range(2, len(sys.argv)):
        curr_sample = int(sys.argv[i])
        seen_samples.add(curr_sample)
        outF.write(struct.pack('Q', curr_sample))
