#!/usr/bin/env python2.7

import sys
import os
import struct


if len(sys.argv) != 2:
    sys.stderr.write("Usage: print_seq.py <sequence file>" + os.linesep*2)
    sys.exit(-1)

seq = []
distinct = set()
with open(sys.argv[1], 'r') as inF:
    done = False
    while not done:
        buf = inF.read(8)
        if len(buf) < 8:
            done = True
        else:
            seq.append(struct.unpack('Q', buf)[0])
            distinct.add(struct.unpack('Q', buf)[0])
print seq
print len(distinct), "distinct elements!"
