#!/usr/bin/env python2.7

import sys
import os
import struct

def check_full_repetition(word, i, p, n):
    # i: position in word
    # p: candidate period
    # n: number of iterations
    if i+(n+1)*p >= len(word):
        return False
    if word[i+(n-1)*p:i+n*p] == word[i+n*p:i+(n+1)*p]:
        return True
    else:
        return False

def find_partial_repetition(word, pA, pB):
    length = 0
    while word[pA+length] == word[pB+length] and pB+length < len(word):
        length += 1
    return pB + length - 1

def check_if_maximal(rep, repetitions_by_start):
    # 0->start5
    # 1->end
    # 2->period3
    for pos in range(rep[0], -1, -1):
        for other_rep in repetitions_by_start[pos]:
            if rep == other_rep:
                continue
            else:
                if rep[2] == other_rep[2] and other_rep[1] >= rep[1]:
                    return False
    return True

if len(sys.argv) != 2:
    sys.stderr.write("Usage: find_maxreps.py <symbol file>" + os.linesep*2)
    sys.exit(-1)

sym_list = []
reps = {}
reps_by_start = {}
with open(sys.argv[1], 'r') as inF:
    done = False
    while not done:
        buf = inF.read(8)
        if len(buf) < 8:
            done = True
        else:
            sym_list.append(struct.unpack('Q', buf,)[0])

#print sym_list
for i in range(0, len(sym_list)):
    reps_by_start[i] = []
# Find repetitions - the slow way
for i in range(0,len(sym_list)):
    # if i % 10 == 0 and i != 0:
    #     sys.stderr.write("Processed %d symbols!\n" % i)
    # Current batch avoids adding multiple repetitions with the same
    # range and different periods, e.g. [1,2,1,2,1,2,1,2] would
    # otherwise yield [1,2]*4 and [1,2,1,2]*2
    current_batch = set()
    for p in range(1, ((len(sym_list)-i)/2)+1):
        iter_count = 1
        while check_full_repetition(sym_list, i, p, iter_count) == True:
            iter_count += 1
        if iter_count > 1:
            # We found a repetition!
            ext = find_partial_repetition(sym_list, i, i+(iter_count)*p)
            # Repetition: (start, end, period)
            if (i, ext) not in current_batch and check_if_maximal((i, ext, p), reps_by_start) == True:
                reps[(i, ext)] = (i, ext, p)
                reps_by_start[i].append((i, ext, p))
            current_batch.add((i,ext))

for k in sorted(reps):
    rep = reps[k]
    sys.stdout.write("%d %d %d " % rep)
    sys.stdout.write(str(sym_list[rep[0]:rep[1]+1])+'\n')
