#ifndef __KK_REPS_HPP
#define __KK_REPS_HPP

#include "lzOG.h"
#include "repetitions.hpp"

void kk_compute_repetitions(unsigned long len, const unsigned long* input, set<Repetition>& reps);

#endif
