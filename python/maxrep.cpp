#include <list>
#include <Python.h>
#include "KKreps.hpp"

static char module_docstring[] =
    "This module searches for maximal repetitions in a sequence of integers";

static char find_docstring[] =
    "Find maximal repetitions";

static PyObject* find(PyObject *self, PyObject* args);

static PyMethodDef module_methods[] = {
  {"find", find, METH_VARARGS, find_docstring},
  {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initmaxrep(void)
{
    PyObject *m = Py_InitModule3("maxrep", module_methods, module_docstring);
    if (m == NULL)
        return;
}


static PyObject* find(PyObject* self, PyObject* args)
{
  PyObject* listObj; /* The list of longs */
  PyObject* numObj; /* One long */ 

  // ----- Prepare input for processing ----- //
  
  if (! PyArg_ParseTuple(args, "O!", &PyList_Type, &listObj))
    return NULL;

  size_t numElems = PyList_Size(listObj);

  if ( numElems == 0 )
    return NULL;
  
  unsigned long* sequence = new unsigned long[numElems];

  for ( size_t i = 0; i < numElems; i++ )
    {
      numObj = PyList_GetItem(listObj, i);
      sequence[i] = PyLong_AsUnsignedLong(numObj);
    }

  // ----- Find maximal repetitions ----- //
  
  set<Repetition> reps;

  kk_compute_repetitions(numElems, sequence, reps);

  list<Repetition> replist;
  for ( set<Repetition>::iterator r_it = reps.begin(); r_it != reps.end(); r_it++ ) {
     replist.push_back(*r_it);
  }
  replist.sort();

  delete[] sequence;

  // ----- Build tuple list describing repetitions ----- //

  PyObject* retval = PyList_New(replist.size());
  size_t pos = 0;
  for ( list<Repetition>::iterator rl_it = replist.begin(); rl_it != replist.end(); rl_it++, pos++ )
    {
      PyObject* start = PyInt_FromLong(rl_it->start);
      PyObject* end = PyInt_FromLong(rl_it->end);
      PyObject* period = PyInt_FromLong(rl_it->period);

      PyObject* tuple = PyTuple_New(3);
      PyTuple_SetItem(tuple, 0, start);
      PyTuple_SetItem(tuple, 1, end);
      PyTuple_SetItem(tuple, 2, period);

      PyList_SetItem(retval, pos, tuple);
    }

  return retval;
}
