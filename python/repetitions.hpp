#ifndef __REPETITIONS_H
#define __REPETITIONS_H

#include <set>
#include <vector>

using namespace std;

class Repetition {
 public:
  Repetition(unsigned long start, unsigned long end, unsigned long period)
  {
    this->start = start;
    this->end = end;
    this->period = period;
  }

  bool operator<(const Repetition& r) const
  {
    if ( start != r.start )
      return start < r.start;
    else
      return end < r.end;
  }

  unsigned long length()
  {
    return end-start+1;
  }
  
  unsigned long start;
  unsigned long end;
  unsigned long period;
};

void right_repetitions(unsigned long orig_offset, const unsigned long* xyword,
		       unsigned long len_xy, unsigned long len_y, set<Repetition>& res);

void left_repetitions(unsigned long orig_offset, const unsigned long* xyword,
		      unsigned long len_xy, unsigned long len_x, set<Repetition>& res);

#endif
