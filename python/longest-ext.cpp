#include <assert.h>
#include <iostream>
#include <vector>

using namespace std;


void LP(unsigned long len, const unsigned long* input, vector<unsigned long>& results)
{
  assert(len > 0);
  // To simplify right-repetition, we add a 0 at the end
  results.resize(len+1);
  results[len] = 0;

  results[0] = len;
  if ( len == 1 )
    return;

  long j;
  for ( j = 0; j < len-1 && input[j] == input[j+1]; j++ );
  results[1] = j;

  long k, i;
  for ( k = 1, i = 2; i < len; i++ )
    {
      long l = k + ((long)results[k]) - i;
      if ( l > 0 && results[i-k] != l )
	results[i] = (l < results[i-k] ? l : results[i-k]);
      else
	{
	  j = ( l > 0 ? l : 0 );
	  for ( ; i + j < len && input[i+j] == input[j]; j++ );
	  results[i] = j;
	  k = i;
	}
    }
}


void LS(unsigned long len, const unsigned long* input, vector<unsigned long>& results)
{
  assert(len > 0);
  results.resize(len);

  results[len-1] = len;
  if ( len == 1 )
    return;
  
  long j;
  for ( j = len-1; j > 0 && input[j] == input[j-1]; j-- );
  results[len-2] = len-1-j;

  long k, i;
  for ( k = len-2, i = len-3; i >= 0; i-- )
    {
      long l = i - (k - ((long)results[k]));
      if ( l > 0 && results[(len-1)-(k-i)] != l )
	results[i] = (l < results[(len-1)-(k-i)] ? l : results[(len-1)-(k-i)]);
      else
	{
	  //j = ( l > 0 ? len-1-l : len-1 );
	  j = ( l > 0 ? l : 0 );
	  for ( ; i - j >= 0 && input[i-j] == input[len-1-j]; j++ );
	  results[i] = j;
	  k = i;
	}
    }
}


void LPvw(unsigned long len, unsigned long len_v, const unsigned long* vw_word, vector<unsigned long>& lpv) {
  assert(len > 0);
  assert(len_v > 0);
  // Note: input = vw and we are interested in computing longest prefix extensions for v,
  // referred to w

  unsigned long len_w = len-len_v;
  const unsigned long* w_word = &(vw_word[len_v]);
    
  lpv.resize(len_v);

  // Compute LP for work w
  vector<unsigned long> lpw;
  LP(len_w, w_word, lpw);
  // for ( unsigned c = 0; c < len_w; c++ )
  //   cout << w_word[c] << " ";
  // cout << '\n';
  // for ( unsigned c = 0; c < len_w; c++ )
  //   cout << lpw[c] << " ";
  // cout << '\n';


  long j;
  // Compute LPvw[0]
  for ( j = 0; j < len_w && vw_word[j] == w_word[j]; j++);
  lpv[0] = j;
  
  // Compute the rest of LPvw
  long k, i;
  for ( k = 0, i = 1; i < len_v; i++ )
    {
      long l = k + ((long)lpv[k]) - i;
      if ( l > 0 && lpw[i-k] != l )
	lpv[i] = (l < lpw[i-k] ? l : lpw[i-k]);
      else
	{
	  j = (l > 0 ? l : 0);
	  for ( ; j < len_w && vw_word[i+j] == w_word[j]; j++ );
	  lpv[i] = j;
	  k = i;
	}
    }
}


void LSwv(unsigned long len, unsigned long len_v, const unsigned long* wv_word, vector<unsigned long>& lsv)
{
  assert(len > 0);
  assert(len_v > 0);
  // Note: input = wv and we are interested in computing longest suffix extensions for v,
  // referred to w

  unsigned long len_w = len-len_v;
  const unsigned long* w_word = wv_word;

    
  lsv.resize(len_v);

  // Compute LP for work w
  vector<unsigned long> lsw;
  LS(len_w, w_word, lsw);
  // for ( unsigned c = 0; c < len_w; c++ )
  //   cout << w_word[c] << " ";
  // cout << '\n';
  // for ( unsigned c = 0; c < len_w; c++ )
  //   cout << lsw[c] << " ";
  // cout << '\n';


  long j;
  // Compute LPvw[0]
  for ( j = 0; j < len_w && wv_word[len-1-j] == w_word[len_w-1-j]; j++);
  lsv[len_v-1] = j;
  
  // Compute the rest of LPvw
  long k, i;
  for ( k = len_v-1, i = len_v-2; i >= 0; i-- )
    {
      // l *can* be negative so just to be sure everything in this formula
      // should be signed
      long l = i - (k - (long)lsv[k]);
      // Note, Lothaire uses l>=0 in the condition below but that
      // seems to be a mistake: l expresses the overlap between the
      // suffix ending at k and the subword ending at i. So if l = 0
      // there is no overlap and we cannot use lsw to reach any
      // conclusion
      if ( l > 0 && lsw[(len_w-1)-(k-i)] != l )
	lsv[i] = (l < lsw[(len_w-1)-(k-i)] ? l : lsw[(len_w-1)-(k-i)]);
      else
	{
	  j = (l > 0 ? l : 0);
	  //for ( ; i + j < len_vw && vw_word[i+j] == w_word[j]; j++ );
	  for ( ; j < len_w && wv_word[len_w+i-j] == w_word[len_w-1-j]; j++ );
	  lsv[i] = j;
	  k = i;
	}
    }
}


// void print_vector(unsigned long* vec, unsigned len)
// {
//   cout << "Input: ";
//   for (unsigned i = 0; i < len; i++ )
//     cout << vec[i] << " ";
//   cout << endl;
// }

// void print_spaced_vector(unsigned long* vec, unsigned len, unsigned long len_x)
// {
//   cout << "Input: ";
//   for (unsigned i = 0; i < len_x; i++ )
//     cout << vec[i] << " ";
//   cout << "| ";
//   for ( unsigned i = len_x; i < len; i++ )
//     cout << vec[i] << " ";
//   cout << endl;
// }

// void print_results(vector<unsigned long> vec, unsigned len)
// {
//   cout << "Result: ";
//   for (unsigned i = 0; i < len; i++ )
//     cout << vec[i] << " ";
//   cout << endl;
// }


// int main()
// {
  // unsigned long vec1[] = {1};
  // unsigned long vec2[] = {1,1};
  // unsigned long vec3[] = {1,2};
  // unsigned long vec4[] = {1,1,1};
  // unsigned long vec5[] = {1,1,2};
  // unsigned long vec6[] = {2,1,1};
  // unsigned long vec7[] = {1,2,1};
  // unsigned long vec8[] = {1,1,1,1};
  // unsigned long vec9[] = {1,1,1,1,1};
  // vector<unsigned long> res;

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec2, 2, 1);
  // LPvw(2, 1, vec2, res);
  // print_results(res,1);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec2, 2, 1);
  // LSwv(2, 1, vec2, res);
  // print_results(res,1);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec3, 2, 1);
  // LPvw(2, 1, vec3, res);
  // print_results(res,1);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec3, 2, 1);
  // LSwv(2, 1, vec3, res);
  // print_results(res,1);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec4, 3, 1);
  // LPvw(3, 1, vec4, res);
  // print_results(res,1);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec4, 3, 1);
  // LSwv(3, 2, vec4, res);
  // print_results(res,2);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec8, 4, 2);
  // LPvw(4, 2, vec8, res);
  // print_results(res,2);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec8, 4, 2);
  // LSwv(4, 2, vec8, res);
  // print_results(res,2);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec8, 4, 3);
  // LPvw(4, 3, vec8, res);
  // print_results(res,3);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec8, 4, 3);
  // LSwv(4, 1, vec8, res);
  // print_results(res,1);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec8, 4, 1);
  // LPvw(4, 1, vec8, res);
  // print_results(res,1);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec8, 4, 1);
  // LSwv(4, 3, vec8, res);
  // print_results(res,3);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec9, 5, 1);
  // LPvw(5, 1, vec9, res);
  // print_results(res,1);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec9, 5, 1);
  // LSwv(5, 4, vec9, res);
  // print_results(res,4);

  // cout << "LPvw" << endl;
  // print_spaced_vector(vec4, 3, 2);
  // LPvw(3, 2, vec4, res);
  // print_results(res,2);
  // cout << "LSwv" << endl;
  // print_spaced_vector(vec4, 3, 2);
  // LSwv(3, 1, vec4, res);
  // print_results(res,1);

  // cout << "LP" << endl;
  // print_vector(vec1, 1);
  // LP(1, vec1, res);
  // print_results(res,1);
  // cout << "LS" << endl;
  // print_vector(vec1, 1);
  // LS(1, vec2, res);
  // print_results(res,1);

  // cout << "LP" << endl;
  // print_vector(vec2, 2);
  // LP(2, vec2, res);
  // print_results(res,2);
  // cout << "LS" << endl;
  // print_vector(vec2, 2);
  // LS(2, vec2, res);
  // print_results(res,2);

  // cout << "LP" << endl;
  // print_vector(vec3, 2);
  // LP(2, vec3, res);
  // print_results(res, 2);
  // cout << "LS" << endl;
  // print_vector(vec3, 2);
  // LS(2, vec3, res);
  // print_results(res, 2);

  // cout << "LP" << endl;
  // print_vector(vec4, 3);
  // LP(3, vec4, res);
  // print_results(res, 3);
  // cout << "LS" << endl;
  // print_vector(vec4, 3);
  // LS(3, vec4, res);
  // print_results(res, 3);

  // cout << "LP" << endl;
  // print_vector(vec5, 3);
  // LP(3, vec5, res);
  // print_results(res, 3);
  // cout << "LS" << endl;
  // print_vector(vec5, 3);
  // LS(3, vec5, res);
  // print_results(res, 3);

  // cout << "LP" << endl;
  // print_vector(vec6, 3);
  // LP(3, vec6, res);
  // print_results(res, 3);
  // cout << "LS" << endl;
  // print_vector(vec6, 3);
  // LS(3, vec6, res);
  // print_results(res, 3);

  // cout << "LP" << endl;
  // print_vector(vec7, 3);
  // LP(3, vec7, res);
  // print_results(res, 3);
  // cout << "LS" << endl;
  // print_vector(vec7, 3);
  // LS(3, vec7, res);
  // print_results(res, 3);
  
  // unsigned long vec1[] = {1,2,3,4,5,8,9,1,2,3,10,11,1,2}; // l = 14
  // unsigned long vec2[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1};   // l = 14
  // unsigned long vec3[] = {3,3,3,3,3,1,2,5,6,3,3,3,3,3,1,2,5,6,7,8}; // l = 20

  // unsigned long vec4[] = {2,1,11,10,3,2,1,9,8,5,4,3,2,1}; // l = 14
  // unsigned long vec5[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1};   // l = 14
  // unsigned long vec6[] = {8,7,6,5,2,1,3,3,3,3,3,6,5,2,1,3,3,3,3,3}; // l = 20

  // unsigned long vec7[] = {1,2,3,4,5,2,3,5,4,5,4,0,9,6,9,  5,4,5,4,7,7,7,7,7,7,7};
  // unsigned long vec8[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,  1,1,1,1,1,1,1,1,1,1,1};
  // unsigned long vec9[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,  16,17,18,19,20,21,22,23,24,25,26};
  // unsigned long vec10[]= {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,  1,2,3,4,5,6,7,8,9,10,11};

  // unsigned long vec11[] = {7,7,7,7,7,7,7,4,5,4,5,  9,6,9,0,4,5,4,5,3,2,5,4,3,2,1};
  // unsigned long vec12[] = {1,1,1,1,1,1,1,1,1,1,1,  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  // unsigned long vec13[] = {26,25,24,23,22,21,20,19,18,17,16,  15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
  // unsigned long vec14[] = {11,10,9,8,7,6,5,4,3,2,1,  15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};

  
  // vector<unsigned long> res1, res2, res3, res4, res5, res6;
  // vector<unsigned long> res7, res8, res9, res10, res11, res12, res13, res14;

  // LPvw(26, 15, vec7, res7);
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << vec7[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 15; i < 26; i++ )
  //   cout << vec7[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res7[i] << " ";
  // cout << '\n';

  // LPvw(26, 15, vec8, res8);
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << vec8[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 15; i < 26; i++ )
  //   cout << vec8[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res8[i] << " ";
  // cout << '\n';

  // LPvw(26, 15, vec9, res9);
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << vec9[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 15; i < 26; i++ )
  //   cout << vec9[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res9[i] << " ";
  // cout << '\n';

  // LPvw(26, 15, vec10, res10);
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << vec10[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 15; i < 26; i++ )
  //   cout << vec10[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res10[i] << " ";
  // cout << '\n';

//   cout << "=================================\n";

  // LSwv(26, 15, vec11, res11);
  // for ( unsigned int i = 0; i < 11; i++ )
  //   cout << vec11[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 11; i < 26; i++ )
  //   cout << vec11[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res11[i] << " ";
  // cout << '\n';

  // LSwv(26, 15, vec12, res12);
  // for ( unsigned int i = 0; i < 11; i++ )
  //   cout << vec12[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 11; i < 26; i++ )
  //   cout << vec12[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res12[i] << " ";
  // cout << '\n';

  // LSwv(26, 15, vec13, res13);
  // for ( unsigned int i = 0; i < 11; i++ )
  //   cout << vec13[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 11; i < 26; i++ )
  //   cout << vec13[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res13[i] << " ";
  // cout << '\n';

  // LSwv(26, 15, vec14, res14);
  // for ( unsigned int i = 0; i < 11; i++ )
  //   cout << vec14[i] << " ";
  // cout << " | ";
  // for ( unsigned int i = 11; i < 26; i++ )
  //   cout << vec14[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 15; i++ )
  //   cout << res14[i] << " ";
  // cout << '\n';

  
  // LP(14, vec1, res1);
  // LP(14, vec2, res2);
  // LP(20, vec3, res3);

  // for ( unsigned int i = 0; i < 14; i++ )
  //   cout << vec1[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 14; i++ )
  //   cout << res1[i] << " ";
  // cout << '\n';
  
  // for ( unsigned int i = 0; i < 14; i++ )
  //   cout << vec2[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 14; i++ )
  //   cout << res2[i] << " ";
  // cout << '\n';

  // for ( unsigned int i = 0; i < 20; i++ )
  //   cout << vec3[i] << " ";
  // cout << '\n';
  // for ( unsigned int i = 0; i < 20; i++ )
  //   cout << res3[i] << " ";
  // cout << '\n';

//   // cout << "====================================\n";
  
//   LS(14, vec4, res4);
//   LS(14, vec5, res5);
//   LS(20, vec6, res6);

//   for ( unsigned int i = 0; i < 14; i++ )
//     cout << vec4[i] << " ";
//   cout << '\n';
//   for ( unsigned int i = 0; i < 14; i++ )
//     cout << res4[i] << " ";
//   cout << '\n';
  
//   for ( unsigned int i = 0; i < 14; i++ )
//     cout << vec5[i] << " ";
//   cout << '\n';
//   for ( unsigned int i = 0; i < 14; i++ )
//     cout << res5[i] << " ";
//   cout << '\n';

//   for ( unsigned int i = 0; i < 20; i++ )
//     cout << vec6[i] << " ";
//   cout << '\n';
//   for ( unsigned int i = 0; i < 20; i++ )
//     cout << res6[i] << " ";
//   cout << '\n';

//   return 0;
// }

