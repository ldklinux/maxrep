#include "KKreps.hpp"
#include <iostream>
#include <list>
#include <map>

#define KKREPS_DEBUG_PRINT 0

static inline unsigned long max(unsigned long a, unsigned long b)
{
  return (a > b ? a : b);
}


static inline unsigned long min(unsigned long a, unsigned long b)
{
  return (a < b ? a : b);
}


void kk_compute_repetitions(unsigned long len, const unsigned long* input, set<Repetition>& reps)
{
  if ( KKREPS_DEBUG_PRINT )
    {
      cout << "Input:";
      for ( unsigned i = 0; i < len; i++ )
	cout << " " << input[i];
      cout << endl << endl;
    }
  
  vector<LONGINT> lzf_offsets;
  vector<LONGINT> lzf_lengths;
  vector<LONGINT> factors_pos;
  vector<LONGINT> factors_len;

  // Step 0 - compute s-factors from input string
  lz_factorise(len, input, lzf_offsets, lzf_lengths);

  // Translate the instruction output of lz_factorize into more
  // manageable position, length pairs
  factors_pos.resize(lzf_offsets.size(), 0);
  factors_len.resize(lzf_offsets.size(), 0);

  unsigned long curroffset = 0;
  for ( unsigned f = 0; f < lzf_lengths.size(); f++ )
    {
      // Remember, if lzf_lengths[f] == 0, then lzf_offsets stores a symbol
      // If lzf_lengths[f] > 0,then lzf_offsets stores the index of the pointed instruction
      factors_pos[f] = curroffset;
      factors_len[f] = (lzf_lengths[f] == 0 ? 1 : lzf_lengths[f]);
      curroffset += factors_len[f];
    }

  if ( KKREPS_DEBUG_PRINT )
    {
      for ( unsigned i = 0; i < lzf_lengths.size(); i++ )
	{
	  cout << "Factor #" << i << ": (" << lzf_offsets[i] << ", " << lzf_lengths[i] << ", "
	       << factors_pos[i] << ", " << factors_len[i] << ") / ";
	  for ( unsigned h = 0; h < factors_len[i]; h++ )
	    cout << input[factors_pos[i]+h];
	  cout << endl;
	}
    }
  


  // Step 1 - find repetitions of type 1
  //set<Repetition> reps;
  // Special handling of f = 1 as there is no f-2 factor
  right_repetitions(0, input, factors_len[0]+factors_len[1], factors_len[1], reps);
  left_repetitions(0, input, factors_len[0]+factors_len[1], factors_len[0], reps);
  for ( unsigned i = 2; i < factors_pos.size(); i++ )
    {
      unsigned long left_ext = min(2*factors_len[i-1] + max(2*factors_len[i-2], factors_len[i]), factors_pos[i]);
      unsigned long start_pos = factors_pos[i] - left_ext;
      unsigned long length = left_ext + factors_len[i];
      right_repetitions(start_pos, &input[start_pos], length, factors_len[i], reps);
      left_repetitions(start_pos, &input[start_pos], length, left_ext, reps);
    }

  list<Repetition> replist;
  if ( KKREPS_DEBUG_PRINT )
    {
      for ( set<Repetition>::iterator r_it = reps.begin(); r_it != reps.end(); r_it++ )
	replist.push_back(*r_it);
      replist.sort();
      for ( list<Repetition>::iterator l_it = replist.begin(); l_it != replist.end(); l_it++ )
	{
	  cout << "Type-1 repetition start: " << l_it->start << " end: " << l_it->end
	       << " period: " << l_it->period << " content:";
	  for ( unsigned i = l_it->start; i <= l_it->end; i++ )
	    cout << " " << input[i];
	  cout << endl;
	}
    }


  // Step 2 - find repetitions of type 2
  
  // Step 2.1 - This list will be used to sort repetitions into lists
  // by end position
  map<unsigned long, list<Repetition> > end;
  for ( set<Repetition>::iterator r_it = reps.begin(); r_it != reps.end(); r_it++ )
    {
      end[r_it->end].push_back(*r_it);
    }
  // Step 2.2 - This list will be used to sort repetitions into lists
  // by start position
  map<unsigned long, list<Repetition> > start;
  for ( unsigned long l = 0; l < len; l++ )
    {
      for ( list<Repetition>::iterator l_it = end[l].begin(); l_it != end[l].end(); l_it++ )
  	{
  	  start[l_it->start].push_back(*l_it);
  	}
    }
  // Step 2.3 - Search for type-2 repetitions to the right of the respective
  // type-1 repetitions
  // (We start from the 2nd factor because there has to be some factor
  // on the left of the current one for the operation to work)
  for ( unsigned i = 2; i < factors_pos.size(); i++ )
    {
      // Repetitions can only occur inside factors whose length is > 2
      // (repetition must end *before* word end
      if ( factors_len[i] < 3 )
	continue;
      // Get backpointer to position of previous s-factor if any
      // (note, backptr is an actual position in the input sequence,
      // not the index of an s-factor)
      unsigned long backptr;
      if ( lzf_lengths[i] > 0 )
	backptr = lzf_offsets[i];
      else
	continue;
      unsigned long delta = factors_pos[i] - backptr;
      // For each position in the previous factor...
      for ( unsigned p = factors_pos[i]+1; p < factors_pos[i] + factors_len[i]; p++ )
	{
	  for ( list<Repetition>::iterator s_it = start[p-delta].begin(); s_it != start[p-delta].end(); s_it++ )
	    {
	      // If the repetition is within the end of the current factor
	      // (Note, it must terminate before the last position!)
	      if ( s_it->length() < (factors_pos[i]+factors_len[i]-p) )
		{
		  start[p].push_back(Repetition(p, p+s_it->length()-1, s_it->period));
		  reps.insert(Repetition(p, p+s_it->length()-1, s_it->period));
		}	      
	    }
	}
    }

  if ( KKREPS_DEBUG_PRINT )
    {
      replist.clear();
      for ( set<Repetition>::iterator r_it = reps.begin(); r_it != reps.end(); r_it++ )
	replist.push_back(*r_it);
      for ( list<Repetition>::iterator l_it = replist.begin(); l_it != replist.end(); l_it++ )
	{
	  cout << "Repetition start: " << l_it->start << " end: " << l_it->end
	       << " period: " << l_it->period << " content:";
	  for ( unsigned i = l_it->start; i <= l_it->end; i++ )
	    cout << " " << input[i];
	  cout << endl;
	}
    }

}
