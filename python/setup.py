from distutils.core import setup, Extension

setup(
    ext_modules=[Extension("maxrep", ["maxrep.cpp", "KKreps.cpp", "longest-ext.cpp", "lzOG.cpp", "repetitions.cpp"])],
)
